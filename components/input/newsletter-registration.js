import classes from "./newsletter-registration.module.css";
import { Fragment, useRef, useContext } from "react";
import { useMutation } from "react-query";
import NotificationContext from "../../store/notification-context";
function NewsletterRegistration() {
	const notificationContext = useContext(NotificationContext);
	const emailRef = useRef();
	const { mutate, isLoading } = useMutation(
		(email) => {
			notificationContext.showNotification({
				title: "Signing up",
				message: "Registering for newsletter",
				status: "pending",
			});
			return fetch("/api/newsletter", {
				method: "POST",
				body: JSON.stringify({ email }),
				headers: {
					"Content-type": "application/json",
				},
			});
		},
		{
			onSuccess: async (data) => {
				const response = await data.json();
				if (data.status === 201) {
					notificationContext.showNotification({
						title: "Success",
						message:
							response?.message ??
							"Successfully registering for newsletter",
						status: "success",
					});
				} else {
					notificationContext.showNotification({
						title: "Failed",
						message: response?.message ?? "Something went wrong!",
						status: "error",
					});
				}
			},
		}
	);
	function registrationHandler(event) {
		event.preventDefault();
		const inputEmail = emailRef.current.value;
		// fetch user input (state or refs)
		// optional: validate input
		// send valid data to API
		mutate(inputEmail);
	}
	return (
		<section className={classes.newsletter}>
			{isLoading ? (
				<p>Loading...</p>
			) : (
				<Fragment>
					<h2>Sign up to stay updated!</h2>
					<form onSubmit={registrationHandler}>
						<div className={classes.control}>
							<input
								type="email"
								id="email"
								placeholder="Your email"
								aria-label="Your email"
								ref={emailRef}
							/>
							<button>Register</button>
						</div>
					</form>
				</Fragment>
			)}
		</section>
	);
}

export default NewsletterRegistration;
