import { useState, useContext } from "react";
import { useMutation, useQueryClient } from "react-query";
import NotificationContext from "../../store/notification-context";
import CommentList from "./comment-list";
import NewComment from "./new-comment";
import classes from "./comments.module.css";
import { AnimatePresence, motion } from "framer-motion";
function Comments(props) {
	const notificationContext = useContext(NotificationContext);
	const { eventId } = props;
	const client = useQueryClient();
	const { mutate } = useMutation(
		(data) => {
			notificationContext.showNotification({
				title: "Adding",
				message: "Loading...",
				status: "pending",
			});
			return fetch(`/api/comments/${eventId}`, {
				method: "POST",
				body: JSON.stringify(data),
				headers: { "Content-Type": "application/json" },
			});
		},
		{
			onSuccess: async (data) => {
				const response = await data.json();
				if (data.status === 201) {
					client.invalidateQueries(["comment", eventId]);
					notificationContext.showNotification({
						title: "Success",
						message: response?.message ?? "Successfully added",
						status: "success",
					});
				} else {
					notificationContext.showNotification({
						title: "Failed",
						message: response?.message ?? "Something went wrong!",
						status: "error",
					});
				}
			},
		}
	);
	const [showComments, setShowComments] = useState(false);

	function toggleCommentsHandler() {
		setShowComments((prevStatus) => !prevStatus);
	}

	function addCommentHandler(commentData) {
		console.log(123);
		mutate(commentData);
	}

	return (
		<section className={classes.comments}>
			<button onClick={toggleCommentsHandler}>
				{showComments ? "Hide" : "Show"} Comments
			</button>
			<AnimatePresence>
				{showComments && (
					<motion.div
						initial={{ opacity: 0 }}
						animate={{ opacity: 1 }}
						exit={{ opacity: 0 }}
						layout
					>
						<NewComment onAddComment={addCommentHandler} />
						<CommentList eventId={eventId} />
					</motion.div>
				)}
			</AnimatePresence>
		</section>
	);
}

export default Comments;
