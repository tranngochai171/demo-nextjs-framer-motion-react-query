import classes from "./comment-list.module.css";
import { useQuery } from "react-query";

function CommentList({ eventId }) {
	const { data, isFetching } = useQuery(["comment", eventId], async () => {
		const res = await fetch(`/api/comments/${eventId}`);
		return await res.json();
	});
	return (
		<ul className={classes.comments}>
			{isFetching && <p>Loading...</p>}
			{data &&
				data?.comments?.map((item) => (
					<li key={item._id}>
						<p>{item.text}</p>
						<div>
							By <address>{item.name}</address>
						</div>
					</li>
				))}
		</ul>
	);
}

export default CommentList;
