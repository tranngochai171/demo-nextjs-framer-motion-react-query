import EventItem from "./event-item";
import classes from "./event-list.module.css";
const EventList = ({ items }) => {
    return (
        <ul className={classes.list}>
            {items.map((event, index) => (
                <EventItem key={event.id} event={event} index={index} />
            ))}
        </ul>
    );
};

export default EventList;
