import { motion } from "framer-motion";
import DateIcon from "../icons/date-icon";
import AddressIcon from "../icons/address-icon";
import ArrowRightIcon from "../icons/arrow-right-icon";
import Button from "../ui/button";
import classes from "./event-item.module.css";
import Image from "next/image";
const EventItem = ({ event: { title, image, location, date, id }, index }) => {
	const humanReadableDate = new Date(date).toLocaleDateString("en-US", {
		day: "numeric",
		month: "long",
		year: "numeric",
	});
	const formattedAddress = location.replace(", ", "\n");
	const exploreLink = `/events/${id}`;
	return (
		<motion.li
			variants={{
				hidden: (index) => ({
					opacity: 0,
					y: index * -100,
				}),
				visible: (index) => ({
					opacity: 1,
					y: 0,
					transition: {
						delay: index * 0.25,
					},
				}),
			}}
			custom={index}
			initial="hidden"
			animate="visible"
			className={classes.item}
		>
			<Image src={`/${image}`} alt={title} width={250} height={160} />
			{/* <img src={`/${image}`} alt={title} /> */}
			<div className={classes.content}>
				<div className={classes.summary}>
					<h2>{title}</h2>
					<div className={classes.date}>
						<DateIcon />
						<time>{humanReadableDate}</time>
					</div>
					<div className={classes.address}>
						<AddressIcon />
						<address>{formattedAddress}</address>
					</div>
				</div>
				<div className={classes.actions}>
					<Button link={exploreLink}>
						<span>Explore Event</span>
						<span className={classes.icon}>
							<ArrowRightIcon />
						</span>
					</Button>
				</div>
			</div>
		</motion.li>
	);
};

export default EventItem;
