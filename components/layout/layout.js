import { Fragment, useContext } from "react";
import MainHeader from "./main-header";
import Notification from "../ui/notification";
import NotificationContext from "../../store/notification-context";
const Layout = ({ children }) => {
	const context = useContext(NotificationContext);
	return (
		<Fragment>
			<MainHeader />
			<main>{children}</main>
			{context.notification && <Notification {...context.notification} />}
		</Fragment>
	);
};

export default Layout;
