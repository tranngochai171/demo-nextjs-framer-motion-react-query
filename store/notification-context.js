import { createContext, useState } from "react";

const NotificationContext = createContext({
	notification: null, // {title, status, message}
	showNotification: function (notificationData) {},
	hideNotification: function () {},
});

export const NotificationContextProvider = ({ children }) => {
	const [activeNotification, setActiveNotification] = useState();
	const showNotificationHandler = (notificationData) => {
		setActiveNotification(notificationData);
	};
	const hideNotificationHandler = () => {
		setActiveNotification(null);
	};

	const context = {
		notification: activeNotification,
		showNotification: showNotificationHandler,
		hideNotification: hideNotificationHandler,
	};

	return (
		<NotificationContext.Provider value={context}>
			{children}
		</NotificationContext.Provider>
	);
};

export default NotificationContext;
