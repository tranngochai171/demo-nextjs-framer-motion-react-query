export async function getAllEvents() {
	const res = await fetch(
		"https://next-js-demo-5abfb-default-rtdb.asia-southeast1.firebasedatabase.app/events.json"
	);
	const data = await res.json();
	const list = [];
	for (const key in data) {
		list.push({ id: key, ...data[key] });
	}
	return list;
}

export async function getFeaturedEvents() {
	const events = await getAllEvents();
	return events.filter((event) => event.isFeatured);
}

export async function getEventById(id) {
	const events = await getAllEvents();
	return events.find((event) => event.id === id);
}

export async function getFilteredEvents(dateFilter) {
	const { year, month } = dateFilter;
	const events = await getAllEvents();

	let filteredEvents = events.filter((event) => {
		const eventDate = new Date(event.date);
		return (
			eventDate.getFullYear() === year &&
			eventDate.getMonth() === month - 1
		);
	});

	return filteredEvents;
}
