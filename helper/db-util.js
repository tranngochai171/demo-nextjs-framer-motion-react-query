import { MongoClient } from "mongodb";
export async function connectDatabase() {
	const client = await MongoClient.connect(
		"mongodb+srv://tranngochai171:hai123789@cluster0.4g72j.mongodb.net/events?retryWrites=true&w=majority"
	);
	return client;
}

export async function insertDocument(client, collection, document) {
	const db = client.db();
	const result = await db.collection(collection).insertOne(document);
	return result;
}

export async function getAllDocuments(client, collection, filter, sort) {
	const db = client.db();
	const document = await db
		.collection(collection)
		.find(filter)
		.sort(sort)
		.toArray();
	return document;
}
