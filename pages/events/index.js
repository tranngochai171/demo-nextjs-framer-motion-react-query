import Head from "next/head";
import EventList from "../../components/events/event-list";
import { getAllEvents } from "../../helper/api-util";
import EventSearch from "../../components/events/event-search";
import { useRouter } from "next/router";
const AllEventsPage = ({ allEvents }) => {
	const router = useRouter();
	// const allEvents = getAllEvents();
	const findEventsHandler = (year, month) => {
		router.push({
			pathname: `/events/[year]/[month]`,
			query: {
				year,
				month,
			},
		});
	};
	return (
		<div>
			<Head>
				<title>All Events</title>
				<meta
					name="description"
					content="Find a lot great events that allow you to evolve..."
				/>
			</Head>
			<EventSearch onSearch={findEventsHandler} />
			<EventList items={allEvents} />
		</div>
	);
};

export async function getStaticProps(context) {
	const allEvents = await getAllEvents();
	return {
		props: {
			allEvents,
		},
		revalidate: 60,
	};
}

export default AllEventsPage;
