import Head from "next/head";
import { Fragment } from "react";
import { useRouter } from "next/router";
import { getFeaturedEvents, getEventById } from "../../helper/api-util";
import EventSummary from "../../components/event-detail/event-summary";
import EventLogistics from "../../components/event-detail/event-logistics";
import EventContent from "../../components/event-detail/event-content";
import Comment from "../../components/input/comment";
import { motion } from "framer-motion";

// import ErrorAlert from "../../compone	nts/ui/error-alert";

const EventDetailPage = ({ event }) => {
	let content = (
		<div className="center">
			<p>Loading...</p>
		</div>
	);
	if (event) {
		content = (
			<Fragment>
				<EventSummary event={event} />
				<EventLogistics event={event} />
				<EventContent>
					<p>{event.description}</p>
				</EventContent>
				<Comment eventId={event.id} />
			</Fragment>
		);
	}
	return (
		<motion.div
			initial={{ opacity: 0 }}
			animate={{ opacity: 1, transition: { delay: 0.25 } }}
		>
			<Head>
				<title>{event.title}</title>
				<meta name="description" content={event.description} />
			</Head>
			{content}
		</motion.div>
	);
};

export async function getStaticPaths() {
	const events = await getFeaturedEvents();
	return {
		paths: events.map((event) => ({
			params: { eventId: event.id },
		})),
		fallback: true,
	};
}

export async function getStaticProps(context) {
	const { params } = context;
	const { eventId } = params;
	const event = await getEventById(eventId);
	if (!event) {
		return { notFound: true };
	}
	return {
		props: {
			event,
		},
	};
}

export default EventDetailPage;
