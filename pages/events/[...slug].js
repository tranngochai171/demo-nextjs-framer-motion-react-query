import Head from "next/head";
import { Fragment } from "react";
import { useRouter } from "next/router";
import EventList from "../../components/events/event-list";
import ResultsTitle from "../../components/events/results-title";
import { getFilteredEvents } from "../../helper/api-util";
import Button from "../../components/ui/button";
import ErrorAlert from "../../components/ui/error-alert";
import { motion } from "framer-motion";
const FilteredEventsPage = (props) => {
	const router = useRouter();
	// const filterData = router.query.slug;
	// if (!filterData) {
	// 	return <p className="center">Loading...</p>;
	// }
	// const [year, month] = filterData;
	const pageHeadData = (
		<Head>
			<title>Filtered Events</title>
			<meta
				name="description"
				content={`All events for ${props.date.month}/${props.date.year}`}
			/>
		</Head>
	);
	if (props.hasError) {
		return (
			<Fragment>
				{pageHeadData}
				<ErrorAlert>
					<p>Invalid filter. Please adjust your values!</p>
				</ErrorAlert>
			</Fragment>
		);
	}
	const filteredEvents = props.filteredEvents;

	if (!filteredEvents || filteredEvents.length === 0) {
		return (
			<Fragment>
				{pageHeadData}
				<ErrorAlert>
					<p>No Events found for the chosen filter!</p>
				</ErrorAlert>
				<div className="center">
					<Button link="/events">Show All Events</Button>
				</div>
			</Fragment>
		);
	}
	const date = new Date(props.date.year, props.date.month - 1);
	return (
		<motion.div
			initial={{ opacity: 0 }}
			animate={{ opacity: 1 }}
			transition={{ delay: 0.5 }}
		>
			{pageHeadData}
			<ResultsTitle date={date} />
			<EventList items={filteredEvents} />
		</motion.div>
	);
};

export async function getServerSideProps(context) {
	const { params } = context;
	const filterData = params.slug;
	const [year, month] = filterData;
	if (
		isNaN(+year) ||
		isNaN(+month) ||
		+year > 2030 ||
		+year < 2021 ||
		+month < 1 ||
		+month > 12
	) {
		return {
			props: { hasError: true },
			// notFound: true
		};
	}
	const filteredEvents = await getFilteredEvents({
		year: +year,
		month: +month,
	});
	return {
		props: {
			filteredEvents,
			date: {
				year: +year,
				month: +month,
			},
		},
	};
}

export default FilteredEventsPage;
