import Head from "next/head";
import { getFeaturedEvents } from "../helper/api-util";
import EventList from "../components/events/event-list";
import NewsletterRegistration from "../components/input/newsletter-registration";
const Home = ({ featuredEvents }) => {
	return (
		<div>
			<Head>
				<title>NextJS Events</title>
				<meta
					name="description"
					content="Find a lot great events that allow you to evolve..."
				/>
			</Head>
			<NewsletterRegistration />
			<EventList items={featuredEvents} />
		</div>
	);
};

export default Home;

export async function getStaticProps(context) {
	const featuredEvents = await getFeaturedEvents();
	return {
		props: {
			featuredEvents,
		},
		revalidate: 1800,
	};
}
