import Layout from "../components/layout/layout";
import "../styles/globals.css";
import Head from "next/head";
import { QueryClient, QueryClientProvider } from "react-query";
const queryClient = new QueryClient();
import { NotificationContextProvider } from "../store/notification-context";
function MyApp({ Component, pageProps }) {
	return (
		<NotificationContextProvider>
			<QueryClientProvider client={queryClient}>
				<Layout>
					<Head>
						<title>NextJs Events</title>
						<meta name="description" content="NextJs Events" />
						<meta
							name="viewport"
							content="initial-scale=1.0, width=device-width"
						/>
					</Head>
					<Component {...pageProps} />
				</Layout>
			</QueryClientProvider>
		</NotificationContextProvider>
	);
}

export default MyApp;
