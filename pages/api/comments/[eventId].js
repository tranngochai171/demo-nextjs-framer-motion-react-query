import {
	insertDocument,
	connectDatabase,
	getAllDocuments,
} from "../../../helper/db-util";

async function handler(req, res) {
	const eventId = req.query.eventId;
	let client;
	try {
		client = await connectDatabase();
	} catch (error) {
		res.status(500).json({ message: "Connecting to database failed!" });
		return;
	}
	if (req.method === "POST") {
		// add server side validation

		const { email, name, text } = req.body;
		if (
			!email ||
			!email.includes("@") ||
			!name ||
			!name.trim() === "" ||
			!text ||
			!text.trim() === ""
		) {
			res.status(422).json({ message: "Invalid data" });
			client.close();
			return;
		}

		const newComment = {
			email,
			name,
			text,
			eventId,
		};
		try {
			const result = await insertDocument(client, "comments", newComment);
			newComment._id = result.insertedId;
			res.status(201).json({ message: "Added comment.", newComment });
		} catch (error) {
			res.status(500).json({ message: "Inserting comment failed!" });
		}
	} else if (req.method === "GET") {
		try {
			const document = await getAllDocuments(
				client,
				"comments", // collection
				{ eventId }, // filter
				{ _id: -1 } // sort
			);
			res.status(200).json({ comments: document });
		} catch (error) {
			res.status(500).json({ message: "Getting comments failed!" });
		}
	}
	client.close();
}

export default handler;
